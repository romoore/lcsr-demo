# Owl Platform Temperature Demo for LCSR #

## Description ##
A quick and simple demonstration of how to use Owl Platform to replace the
current temperature monitoring page for LCSR at Rutgers University.  Uses the
Owl Platform web API and a little jQuery.

## Contributors ##
Robert Moore - Developer
