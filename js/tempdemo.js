var tid;
var pollDelay = 60000;
var tempMin = 50;
var tempRange = 35;

var colors = ['#00c','#00f','#39f','#3ff',/* Cool colors (blue) */
		'#0c0','#0f0','#8f0', '#cc0', /* Warmer colors (green, yellow) */
		'#f93','#ff8000','#f66','#f00']; /* Hot colors (orange, red) */

var tempColor = function(temp) {
	var adjusted = temp - tempMin;
	if(adjusted < 0){
		adjusted = 0;
	}else if(adjusted > tempRange){
		adjusted = tempRange;
	}

	var scaled = (adjusted/tempRange)*15;
	/*return colors[Math.round(scaled)]; */
	return '#' + toHex(scaled) + 0 + toHex(15-scaled); 
}

var hexVals = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];

var toHex = function(decimal){
	return hexVals[Math.floor(decimal)];
}

var chartReady = false;
var chartDiv = $('#temp-chart');
var chartHeader = $('#temp-chart-head');
var unHide = true;

var chart = null;
var chartOptions = {
	labels: ['Date', 'Temperature (F)']
}

var markChartLoaded = function (){
	chartReady = true;
}

var main = function() {
	//
	pollStatus();
	tid = setInterval(pollStatus,pollDelay);
}

var pollStatus = function() {
	$.getJSON("http://grail.rutgers.edu:8011/grailrest/snapshot?q=lcsr.temp.*&a=temperature&cb=?",
		function(data){
			updateScreen(data);
		});
	
}

var updateScreen = function(attributesArr){

	if(attributesArr !== null){
		$.each(attributesArr, function(index, value){
			var id = value.identifier.split('.')[2];
			var temp = Math.round(value.attributes[0].data);
			var $div = $('#temp-'+id);
			var $anchor = $('#a-'+id);
			if(!$anchor.length){
				$anchor = $('<a href="#" id="a-'+id+'"></a>');
				$anchor.appendTo($div);
				$anchor.click(function(evt){evt.preventDefault();showChart(id)});
			}
			$anchor.text(temp);
			var color = tempColor(temp);
			$div.css('background-color',color);
		});
	}

}

var showChart = function(sensorId) {
	NProgress.start();
	var now = (new Date()).getTime();
	var dayAgo = now - 24*60*60*1000;
	if(unHide){
		unHide = false;
		chartHeader.removeClass('invisible');
		chartDiv.removeClass('invisible');
		$('#temp-chart-tips').removeClass('invisible');
	}
	chartHeader.text('Sensor ' + sensorId);
	$.getJSON("http://grail.rutgers.edu:8011/grailrest/range?q=lcsr.temp."+sensorId+"&a=temperature&st=0&et="+now+"&cb=?",
		function(data){
			if(data.length){
				NProgress.set(.1);
				renderChart(data[0]);
			}else{
				NProgress.done();
			}
		});
}

var renderChart = function(history){
	var identifier;
//	var now = (new Date()).getTime();
//	var dayAgo = now - 24*60*60*1000;
		var minTemp = 40;
		var maxTemp = 100;
		var chartData = [];
			$.each(history, function(index, value){
				NProgress.inc();
				if(index === 'identifier'){
					identifier = value;
				}else if(index === 'attributes'){
					$.each(value, function(idx, attr){
//						var ts = (Date.parse(attr.creationDate)-now)/(3600000);
						var t = Math.round(attr.data);
						if(t > maxTemp){
							maxTemp = t+1;
						}
						if(t < minTemp){
							minTemp = t-1;
						}
						chartData.push([new moment(attr.creationDate).toDate(),t]);
					});
				}
				
			});
		
			if(chart == null){
				chart = new Dygraph(document.getElementById("temp-chart"),chartData,chartOptions);
			}else{
				chartOptions['file'] = chartData;
				chart.updateOptions(chartOptions);
			}
			NProgress.done();

}	

$(document).ready(main);
