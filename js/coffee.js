var tid;
var pollDelay = 60000;
var tempMin = 70;
var tempRange = 25;

var colors = ['#00c','#00f','#39f','#3ff',/* Cool colors (blue) */
		'#0c0','#0f0','#8f0', '#cc0', /* Warmer colors (green, yellow) */
		'#f93','#ff8000','#f66','#f00']; /* Hot colors (orange, red) */

var tempColor = function(temp) {
	var adjusted = temp - tempMin;
	if(adjusted < 0){
		adjusted = 0;
	}else if(adjusted > tempRange){
		adjusted = tempRange;
	}

	var scaled = (adjusted/tempRange)*15;
	/*return colors[Math.round(scaled)]; */
	return '#' + toHex(scaled) + 0 + toHex(15-scaled); 
}

var hexVals = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];

var toHex = function(decimal){
	return hexVals[Math.floor(decimal)];
}

var chartReady = false;
var chartDiv = $('#temp-chart');
var chartHeader = $('#temp-chart-head');
var unHide = true;

var chart = null;

var chartOptions = {
	labels: ['Date', 'Temerature (F)']
}

var markChartLoaded = function (){
	chartReady = true;
}

var main = function() {
	//
	pollStatus();
	tid = setInterval(pollStatus,pollDelay);
	chartHeader.addClass('invisible');
	chartDiv.addClass('invisible');
}

var pollStatus = function() {
	$.getJSON("http://grail.rutgers.edu:8011/grailrest/snapshot?q=richm.temp.*&a=temperature.celsius&cb=?",
		function(data){
			updateScreen(data);
		});
	
}

var updateScreen = function(attributesArr){

	if(attributesArr !== null){
		$.each(attributesArr, function(index, value){
			var id = value.identifier.split('.')[2];
			var temp = Math.round(value.attributes[0].data*1.8+32);
			var $div = $('#temp-'+id);
			var $divImg = $('#image-container');
			var $anchor = $('#a-'+id);
			if(!$anchor.length){
				$anchor = $('<a href="#" id="a-'+id+'"></a>');
				$anchor.appendTo($div);
				$anchor.click(function(evt){evt.preventDefault();showChart(id)});
			}
			$anchor.html(temp+'&deg;');
			var color = tempColor(temp);
			$div.css('background-color',color);
			$divImg.css('background-color', color);
		});
	}

}

var showChart = function(sensorId) {

	var now = (new Date()).getTime();
	var dayAgo = now - 24*60*60*1000;
	if(unHide){
		unHide = false;
		chartHeader.removeClass('invisible');
		chartDiv.removeClass('invisible');
	}
	chartHeader.text('Sensor ' + sensorId);
	$.getJSON("http://grail.rutgers.edu:8011/grailrest/range?q=richm.temp."+sensorId+"&a=temperature.celsius&st=0&et="+now+"&cb=?",
		function(data){
			if(data.length){
				renderChart(data[0]);
			}
		});
}

var renderChart = function(history){
	var identifier;
//	var now = (new Date()).getTime();
//	var dayAgo = now - 24*60*60*1000;
		var minTemp = 60;
		var maxTemp = 90;
		var absMaxTemp = 110;
		var absMinTemp = 50;
		var chartData = [];
			$.each(history, function(index, value){
				if(index === 'identifier'){
					identifier = value;
				}else if(index === 'attributes'){
					$.each(value, function(idx, attr){
						
//						var ts = (Date.parse(attr.creationDate)-now)/(3600000);
						var t = Math.round(attr.data*1.8+32);
						if(t > maxTemp){
							maxTemp = t+1;
						}
						if(t < minTemp){
							minTemp = t-1;
						}
						chartData.push([new moment(attr.creationDate).toDate(),t]);
					});
				}
				
			});
			if(maxTemp > absMaxTemp){
				maxTemp = absMaxTemp;
			}
			if(minTemp < absMinTemp){
				minTemp = absMinTemp;
			}

			if(chart == null){
				chart = new Dygraph(document.getElementById("temp-chart"),chartData,chartOptions);
			}else {
				chartOptions['file'] = chartData;
				chart.updateOptions(chartOptions);
			}

}	

$(document).ready(main);
